import sys

def read_word_search_file(file_name):
    with open(file_name, "r") as file:
        read_lines = file.read().splitlines()


    # Get the first line of the file and read the grid size
    matrix_size = read_lines[0].split('x')
    num_of_rows = int(matrix_size[0]) # Get number of rows
    num_of_cols = int(matrix_size[1]) # Get number of columns

    # Get the character grid with the scrambled words
    character_grid = [row.split() for row in read_lines[1:num_of_rows+1]]

    # Get list of words to find
    word_search_list = read_lines[num_of_rows+1:]

    return num_of_rows, num_of_cols, character_grid, word_search_list

def horizontal_search(grid, word, backwards_word):

    found = False
    for row_idx, row in enumerate(grid):
        row_str = ''.join(row) #form a word from the grid row line
        if word in row_str:
            start = row_str.index(word)
            end = start + len(word) - 1
            print(f'{word} {row_idx}:{start} {row_idx}:{end}')
            found = True

        if backwards_word in row_str:
            start = row_str.index(backwards_word) + len(backwards_word) - 1
            end = start - len(backwards_word) + 1
            print(f'{word} {row_idx}:{start} {row_idx}:{end}')
            found = True

    return found

def vertical_search(grid, word, backwards_word):
    # Transpose the grid to do vertical search similar to horizontal
    transpose_grid = list(map(list, zip(*grid)))

    found = False
    for col_idx, col in enumerate(transpose_grid):
        col_str = ''.join(col)

        if word in col_str:
            start = col_str.index(word)
            end = start + len(word) - 1
            print(f'{word} {start}:{col_idx} {end}:{col_idx}')
            found = True

        if backwards_word in col_str:
            start = col_str.index(backwards_word) + len(backwards_word) - 1
            end = start - len(backwards_word) + 1
            print(f'{word} {start}:{col_idx} {end}:{col_idx}')
            found = True

    return found

def diagonal_search(grid, word, backwards_word, num_of_rows, num_of_cols):
    
    found = False
    #limit diagonal search space 
    # (top-left to bottom-right direction)
    # words that are 4 long in a 5 x 5 grid only have a few options for starting positions
    start_row_range = range(num_of_rows - len(word) + 1)
    start_col_range = range(num_of_cols - len(word) + 1)

    # Search for word diagonally (top-left to bottom-right direction and reverse) 
    for start_row in start_row_range:
        for start_col in start_col_range:
            diagonal = ''.join([grid[start_row + i][start_col + i] for i in range(len(word))])

            if word in diagonal:
                end_row = start_row + diagonal.index(word) + len(word) - 1
                end_col = start_col + diagonal.index(word) + len(word) - 1
                print(f'{word} {start_row}:{start_col} {end_row}:{end_col}')
                found = True

            if backwards_word in diagonal:
                end_row = start_row 
                end_col = start_col
                start_row = end_row + diagonal.index(backwards_word) + len(word) - 1
                start_col = end_col + diagonal.index(backwards_word) + len(word) - 1
                print(f'{word} {start_row}:{start_col} {end_row}:{end_col}')                
                found = True

    # Limit diagonal search space to start positions that are viable 
    # (bottom-left to top-right direction)
    # words that are 4 long in a 5 x 5 grid only have a few options for starting positions
    start_row_range = range(len(word) - 1, num_of_rows)
    start_col_range = range(num_of_cols - len(word) + 1)

    # Search for word diagonally (bottom-left to top-right direction)
    for start_row in start_row_range:
        for start_col in start_col_range:
            diagonal = ''.join([grid[start_row - i][start_col + i] for i in range(len(word))])
            if word in diagonal:
                end_row = start_row - diagonal.index(word) - len(word) + 1
                end_col = start_col + diagonal.index(word) + len(word) - 1
                print(f'{word} {start_row}:{start_col} {end_row}:{end_col}')
                found = True

            if backwards_word in diagonal:
                end_row = start_row 
                end_col = start_col
                start_row = end_row - diagonal.index(backwards_word) - len(word) + 1
                start_col = end_col + diagonal.index(backwards_word) + len(word) - 1
                print(f'{word} {start_row}:{start_col} {end_row}:{end_col}')
                found = True
    
    return found

def find_word(grid, word, num_of_rows, num_of_cols):
    #remove spaces for the word search
    word = word.replace(" ", "")
    
    # Create inverse of word to do backwards searches
    backwards_word = word[::-1]
    
    # Horizontal Search
    if horizontal_search(grid, word, backwards_word):
        return

    # Vertical Search
    if vertical_search(grid, word, backwards_word):
        return
    
    # Diagonal Search
    if diagonal_search(grid, word, backwards_word, num_of_rows, num_of_cols):
        return

    print(f'{word} not found')
    return

def main():
    if len(sys.argv) > 1:
        file_name = sys.argv[1]
    else:
        file_name = input("Enter File name: ")
    
    num_of_rows, num_of_cols, grid, search_list = read_word_search_file(file_name)

    for word in search_list:
        if len(word) <= max(num_of_cols,num_of_rows):
            find_word(grid,word,num_of_rows, num_of_cols)
        else:
            print(f'{word} is too large for the grid ({num_of_rows}x{num_of_cols})')

if __name__ == '__main__':
    main()