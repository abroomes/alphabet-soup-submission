import unittest
from io import StringIO
import sys

from word_search import horizontal_search, vertical_search, diagonal_search, find_word

class WordSearchFinderTests(unittest.TestCase):

    def test_search_word_horizontal(self):
        grid = [
            ['H', 'A', 'S', 'D', 'F'], 
            ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'D'], 
            ['C', 'V', 'B', 'L', 'N'], 
            ['G', 'O', 'O', 'D', 'O'], 
            ['S', 'G', 'A', 'R', 'T']
        ]

        word = 'ART'
        expected_output = True
        self.assertEqual(horizontal_search(grid,word,word[::-1]), expected_output)

    def test_search_word_horizontal_fail(self):
        grid = [
            ['H', 'A', 'S', 'D', 'F'], 
            ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'D'], 
            ['C', 'V', 'B', 'L', 'N'], 
            ['G', 'O', 'O', 'D', 'O'], 
            ['S', 'G', 'A', 'R', 'T']
        ]

        word = 'ARF'
        expected_output = False
        self.assertEqual(horizontal_search(grid,word,word[::-1]), expected_output)

    def test_search_word_vertical(self):
        grid = [
            ['H', 'A', 'S', 'D', 'F'], 
            ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'D'], 
            ['C', 'V', 'B', 'L', 'N'], 
            ['G', 'O', 'O', 'D', 'O'], 
            ['S', 'G', 'A', 'R', 'T']
        ]

        word = 'BLYS'
        expected_output = True
        self.assertEqual(vertical_search(grid,word,word[::-1]), expected_output)

    def test_search_word_vertical_fail(self):
        grid = [
            ['H', 'A', 'S', 'D', 'F'], 
            ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'D'], 
            ['C', 'V', 'B', 'L', 'N'], 
            ['G', 'O', 'O', 'D', 'O'], 
            ['S', 'G', 'A', 'R', 'T']
        ]

        word = 'ARF'
        expected_output = False
        self.assertEqual(vertical_search(grid,word,word[::-1]), expected_output)

    def test_search_word_diagonal(self):
        grid = [
            ['H', 'A', 'S', 'D', 'F'], 
            ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'D'], 
            ['C', 'V', 'B', 'L', 'N'], 
            ['G', 'O', 'O', 'D', 'O'], 
            ['S', 'G', 'A', 'R', 'T']
        ]

        word = 'OR'
        expected_output = True
        self.assertEqual(diagonal_search(grid,word,word[::-1],6,5), expected_output)

    def test_search_word_diagonal_fail(self):
        grid = [
            ['H', 'A', 'S', 'D', 'F'], 
            ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'D'], 
            ['C', 'V', 'B', 'L', 'N'], 
            ['G', 'O', 'O', 'D', 'O'], 
            ['S', 'G', 'A', 'R', 'T']
        ]

        word = 'ARF'
        expected_output = False
        self.assertEqual(diagonal_search(grid,word,word[::-1],6,5), expected_output)

    def test_word_not_found_output(self):
        captured_output = StringIO()
        sys.stdout = captured_output

        grid = [
            ['H', 'A', 'S', 'D', 'F'], 
            ['G', 'E', 'Y', 'B', 'H'], 
            ['J', 'K', 'L', 'Z', 'D'], 
            ['C', 'V', 'B', 'L', 'N'], 
            ['G', 'O', 'O', 'D', 'O'], 
            ['S', 'G', 'A', 'R', 'T']
        ]

        word = 'HALSEY'

        find_word(grid, word, 6,5)
        printed_text = captured_output.getvalue()

        expected_text = f"{word} not found"
        self.assertIn(expected_text, printed_text)

        sys.stdout = sys.__stdout__

if __name__ == '__main__':  
    unittest.main()